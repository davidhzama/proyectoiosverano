//
//  MapViewController.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/7/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var capacidad: UILabel!
    @IBOutlet weak var tipo: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var mapa: MKMapView!
    
    var camp : CampModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2D(latitude: camp.lat!, longitude: camp.lon!)
        pin.title = camp.name
        mapa.addAnnotation(pin)
        mapa.setCenter(pin.coordinate, animated: true)
        
        nombre.text = "\("Nombre: ") \(camp.name ?? "")"
        capacidad.text = "\("Capacidad ") \(camp.cap ?? "")"
        precio.text = "\("Precio: ") \(camp.price ?? "")"
        tipo.text = "\("Tipo: ") \(camp.type ?? "")"
      
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
