//
//  CampTableViewCell.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/7/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit

class CampTableViewCell: UITableViewCell {
    
    @IBOutlet weak var campName: UILabel!
    @IBOutlet weak var campCapacity: UILabel!
    @IBOutlet weak var campType: UILabel!
    @IBOutlet weak var campPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(for camp: CampModel)  {
        self.campName.text = camp.name
        self.campCapacity.text = camp.cap
      self.campType.text = camp.type
        self.campPrice.text = camp.price
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
