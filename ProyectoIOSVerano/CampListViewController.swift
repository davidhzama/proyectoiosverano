//
//  CampListViewController.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/7/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var camps = [CampModel]()

class CampListViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
   
    var selectedcamp : CampModel!
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        loadCamps()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return camps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CampTableViewCell") as! CampTableViewCell
        cell.set(for: camps[indexPath.row])
        return cell
    }
    
    func loadCamps() {
        
        AF.request("http://www.mocky.io/v2/5d22b5d72e00005200c3eb68").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
                let json = JSON(data)
                for user in json{
                    camps.append(CampModel(object:user.1))
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedcamp = camps[indexPath.row]
        performSegue(withIdentifier: "ubicacion", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ubicacion"{
            let vc = segue.destination as! MapViewController
            vc.camp = selectedcamp
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
