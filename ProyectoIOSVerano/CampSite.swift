//
//  CampSite.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/7/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//


import Foundation
import SwiftyJSON

struct CampModel {
   // var id: Int?
    var name: String?
    var price: String?
    var cap: String?
    var type: String?
    var lat: Double?
    var lon: Double?
    
    init(name: String?, price: String?, cap: String?, type: String?, lat: Double?, lon: Double?) {
        self.name = name
        self.price = price
        self.cap = cap
        self.type = type
        self.lat = lat
        self.lon = lon
    }
    
    init(object: JSON){
       // id = object["id"].intValue
        name = object["name"].stringValue
        price = object["price"].stringValue
        cap = object["cap"].stringValue
        
        type = object["type"].stringValue
        //Llenar los de lat y lon
        lat = object["address"]["geo"]["lat"].doubleValue
        lon = object["address"]["geo"]["lng"].doubleValue
    }
}
