//
//  AddCampViewController.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/7/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit

class AddCampViewController: UIViewController {
    
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var tipo: UITextField!
    @IBOutlet weak var capcidad: UITextField!
    @IBOutlet weak var precio: UITextField!
    
   

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func hideKeyboard () {
        
        view.endEditing(true)
    }
    
    @IBAction func addCamp(_ sender: Any) {
        
        if (nombre.text !=  "" && tipo.text != "" && capcidad.text != "" && precio.text != ""){
            
            let newCamp = CampModel(name: nombre.text, price: precio.text, cap: capcidad.text, type: tipo.text, lat: 20.744377, lon: -103.378615)
            
            camps.append(newCamp)
            
            let alert = UIAlertController(title: "Nuevo Campamento", message: "Se registro correctamente", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            
        }else{
            
            let alert = UIAlertController(title: "Nuevo campamento", message: "Campos vacios", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

     tasks.append(TaskModel(task: task.text!, dueDate: datePicker.date))
     
     var id: Int?
     var name: String?
     var price: String?
     var cap: String?
     var type: String?
     var lat: Double?
     var lon: Double?
     
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
