//
//  ViewController.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/4/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userLogIn: UITextField!
    @IBOutlet weak var passLogIn: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func logIn(_ sender: Any) {
        
        if(userLogIn.text == UserDefaults.standard.string(forKey: "username")){
            if(passLogIn.text == UserDefaults.standard.string(forKey: "password")){
                
                
                
                if let mainTabBar = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarController") {
                    UIApplication.shared.keyWindow?.rootViewController = mainTabBar
                }
                
             
            
            }else{
                let alertController = UIAlertController(title: "LogIn", message:
                    "Contraseña Invalida", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                
                self.present(alertController, animated: true, completion: nil)
            }
            
        }else{
            let alertController = UIAlertController(title: "LogIn", message:
                "Usuario Invalido", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
       
       
        
        
        }
    
}

