//
//  RegisterViewController.swift
//  ProyectoIOSVerano
//
//  Created by Luis David Hernandez Zamarripa on 7/5/19.
//  Copyright © 2019 Luis David Hernandez Zamarripa. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var newUser: UITextField!
    @IBOutlet weak var NewPassword: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerNewUser(_ sender: Any) {
        
        
        
        if(newUser.text != ""){
            if(NewPassword.text != ""){
                
                UserDefaults.standard.set(newUser.text, forKey: "username")
                UserDefaults.standard.set(NewPassword.text, forKey: "password")
                UserDefaults.standard.synchronize();
                
                
                let alertController = UIAlertController(title: "Registro", message:
                    "Se registro exitosamente", preferredStyle: .alert)
                 alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                
                self.present(alertController, animated: true, completion: nil)
                
                
                
            // let mainTabBar =
                self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController")// {
                   // UIApplication.shared.keyWindow?.rootViewController = mainTabBar
                //}
          
            }else{
                let alertController = UIAlertController(title: "Registro", message:
                    "Ingrese una contraseña", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                
                self.present(alertController, animated: true, completion: nil)
            }
            
            
        }else{
            
            let alertController = UIAlertController(title: "Registro", message:
                "Ingrese un nombre de usuario", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
